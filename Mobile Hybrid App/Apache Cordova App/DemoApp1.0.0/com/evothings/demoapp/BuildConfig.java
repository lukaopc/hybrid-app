package com.evothings.demoapp;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.evothings.demoapp";
    public static final String BUILD_TYPE = "debug";
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 10000;
    public static final String VERSION_NAME = "1.0.0";
}
